from flask import Flask, render_template
from gpiozero import LED

inputs = { 
    "cpanel_sensor_1" : LED(2),
    "cpanel_sensor_2" : LED(3),
    "large_pusher_sensor_1" : LED(4),
    "large_pusher_sensor_2" : LED(5),
    "window_sensor_1" : LED(6),
    "window_sensor_2" : LED(7),
    "bb_sensor" : LED(8),
    "big_wagon_1" : LED(9),
    "big_wagon_2" : LED(10),
    "small_wagon_1" : LED(11),
    "small_wagon_2" : LED(12),
    "back_front_1" : LED(13),
    "back_front_2" : LED(14),
}

history = []

def create_page(): 
    return render_template(
        'index.html',
        history = history,
        inputs = { key : value.is_lit for (key, value) in inputs.items() }) 

app = Flask(__name__)

@app.route('/')
def hello_world():
    #return '<br>'.join(inputs.keys())
    return create_page() 

@app.route('/set/<input>')
def enable_sensor(input):
    if input in inputs:
        inputs[input].on()
        history.append(input + ": on")
    else:
        history.append("invalid set input " + input)
    return create_page()

@app.route('/clear/<input>')
def disable_sensor(input):
    if input in inputs:
        inputs[input].off()
        history.append(input + ": off")
    else:
        history.append("invalid clear input " + input)
    return create_page()

if __name__ == '__main__':
   app.run(debug = True)
